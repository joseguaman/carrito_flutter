import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:validators/validators.dart';

class SessionView extends StatefulWidget {
  const SessionView({ Key? key }) : super(key: key);

  @override
  _SessionViewState createState() => _SessionViewState();
}

class _SessionViewState extends State<SessionView> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController correoControl = TextEditingController();
  final TextEditingController claveControl = TextEditingController();

  void _iniciar() {
    setState(() {
      if(_formKey.currentState!.validate()) {
        log("ok");
      } else {
        log("Errores");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Scaffold(
        body: ListView(
          padding: const EdgeInsets.all(32),
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(10),
              child: const Text("Noticias",
              style: TextStyle(
                color: Colors.blue,
                fontWeight: FontWeight.bold,
                fontSize: 30
              ))
            ),
            Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(10),
              child: const Text("La mejor app de noticias",
              style: TextStyle(                
                fontSize: 20
              ))
            ),
            Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(10),
              child: const Text("Inicio de sesion",
              style: TextStyle(                
                fontSize: 20,
                fontWeight: FontWeight.bold
              ))
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextFormField(
                controller: correoControl,
                decoration: const InputDecoration(
                  labelText: 'Correo',
                  suffixIcon: Icon(Icons.alternate_email)
                ),
                validator: (value){
                  if(value!.isEmpty) {
                    return "Debe ingresar su correo";
                  }
                  if(!isEmail(value)) {
                    return "Debe ingresar un correo valido";
                  }
                },
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              
              child: TextFormField(
                decoration: const InputDecoration(
                  labelText: 'Clave',
                  suffixIcon: Icon(Icons.key)
                ),
                controller: claveControl,
                validator: (value){
                  if(value!.isEmpty) {
                    return "Debe ingresar su clave";
                  }
                },
              ),
            ),
            Container(
              height: 50,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: ElevatedButton(
                child: const Text("Inicio"),
                onPressed: _iniciar
              ),
            ),
          ],
        ),
      ),
    );
  }
}